# kubernetes versuchs- und deployment-cluster

dieses mininmalistische beispiel demonstriert in grundzügen, wie man mit hilfe
von kubernetes bzw. der ausgesprochen ressourcenschonenden implementierung `k3s`
einfache serveranwendungen konfigurieren und abwickeln kann.

alle dazu notwendigen anweisungen sind in den yaml-files im ordner `manifests`
zu finden. änderungen an diesen files werden im regelfall laufend vom system
erkannt und übernommen *(applyd)*, so dass auf diesem weg mit einfachsten
mitteln die vorzüge konsequenter GitOps praxis realisiert werden können.

als beispielapplikation wird im vorliegenden fall die anwendung `video-server`
ausgerollt. das kann man aber jederzeit ändern, indem man die beiden betreffende
files im `manifests` directory löscht bzw. anweisungen für andere
anwendungen an dessen stelle platziert.

## einmalige vorbereitung:

1. neues CLUSTER_SECRET generieren:

       SECRET=`echo $RANDOM$RANDOM$RANDOM | sha256sum | cut -d " " -f1`
       echo "K3S_CLUSTER_SECRET=$SECRET" > .env

2. email adresse für die letsencrypt nutzung setzen:

       EMAIL="deine_anschrift@mur.at" \
         envsubst < manifests/traefik2.yaml | tee manifests/traefik2.yaml

## hilfprograme 

in diesem repositrory finden sich auch einige wichtige hilfsprogramme, die aber
ggf. duch neuere versionen ersetzt werden sollten.

[k3s](https://github.com/rancher/k3s)

    curl -L https://github.com/rancher/k3s/releases/latest/download/k3s  \
      > k3s && chmod a+x k3s

`k3s` wird hier auch für die auführung der befehle `kubectl` und `helm`
herangezogen.

[docker-compose](https://github.com/docker/compose)

    curl -L https://github.com/docker/compose/releases/latest/download/ \
      docker-compose-Linux-x86_64 > docker-compose && chmod a+x docker-compose

## starten:

starten des clusters:

    ./docker-compose up -d

## zugriff:

das passende `kubeconfig.yaml`-file mit den zugriffsadten wird im aktuellen
verzeichnis angelegt.

man kann es etweder über einen hinweis in den umgebungsvariable nutzen:

    export KUBECONFIG=`pwd`/kubeconfig.yaml

oder in dem man es nach `~/.kube/config` symbolisch verlinkt bzw. kopiert.

letzteres funktioniert auch von entferten maschinen aus. in dem fall muss man
aber dort noch den eintrag `server https://127.0.0.1:6443` auf den von außen
erreichbaren namen abändern.

## überprüfen:

    ./kubectl cluster-info

    ./kubectl get all -A -o wide

## stoppen:

um den cluster zu stoppen:

    ./docker-compose down

um die persistenten datenbestände auch noch zu entfernen, müssen zusätzlich noch
named volumes gelöscht werden:

    docker volume rm ms_kube_k3s-server

einfacher ist oft ein:

    docker volume prune

